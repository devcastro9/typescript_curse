/*
Para instalar TypeScript
npm -g typescript

Inicializa el proyecto:
tsc -init

Compila el archivo .ts:
tsc <nombre del archivo>
*/
var saludo = "Hola mundo";
function saludar() {
    console.log(saludo);
}
saludar();
